import argparse
import csv
import logging
import os
from operator import attrgetter
import ConfigParser

logging.basicConfig(level=logging.DEBUG)

class Record:
    """A record containing values collected from the files"""
    def __init__(self, data):
        self.update(data)
        
    def __iter__(self):
        """Iterate over attributes in the record"""
        for attr in dir(self):
            if not attr.startswith("__"):
                yield attr, getattr(self, attr)

    def update(self, data):
        for k,v in data.iteritems():
            setattr(self, k, v)

    def output(self, columns):
        """Return a list containing the data from the supplied columns"""
        # This function is currently broken. 
        # Some columns are not returned.
        if columns:
            cols = columns.split(',')

        res = []
        for col in cols:
            res.append(getattr(self, col, ''))
        return res


class Records:
    """A collection of records"""
    
    records = []

    def __init__(self, config, args):
        self.args = args
        self.config = config
        self.priorities = args.priority.split(',') or ''

    def add(self, record, groupBy):
        """Add a record to the list"""
        if not self.contains(groupBy, record):
            self.records.append(record)
        else:
            for item in self.records:
                i_id = getattr(item, groupBy)
                r_id = getattr(record, groupBy)

                if i_id == r_id:
                    # Here is where priority should be handled
                    for key, val in record:
                        setattr(item, key, val)

    def contains(self, filter, record):
        """Determine if a record has already been added to list"""
        for item in self.records:
            if getattr(item, filter) == getattr(record, filter):
                return True
        return False

    def prioritize(self, file):
        if self.priorities:
            if file in self.priorities:
                return self.priorities.index(file)

            return len(self.priorities) + 1

    def get(self):
        """Walks the supplied path for data read in order of priority"""
        for root, dirs, files in os.walk(self.args.path):
            # sort files based on priority.
            # Need FILO ordering to ensure that the data in higher 
            # priority files overwrites the data in lower priority files.

            files.sort(key=self.prioritize)
            files.reverse()

            for file in files:
                with open(os.path.join(root, file), 'rb') as csvfile:
                    logging.info('Opened "{file}"'.format(**vars()))

                    headers = csvfile.readline().strip().split(', ')
                    reader = csv.DictReader(csvfile, fieldnames=headers)

                    for row in reader:
                        rec = Record(self.clean_row(row))
                        self.add(rec, self.args.groupBy)

    def clean_row(self, row):
        for k, v in row.iteritems():
            row[k] = v.strip()
        return row

    def write(self):
        """Write all records to the output file grouped sorted"""
        with open(self.config.get('defaults', 'output'), 'wb') as outFile:
            cw = csv.writer(outFile,
                        delimiter=self.args.output,
                        quotechar='"',
                        quoting=csv.QUOTE_MINIMAL
                        )

            # Write headers to file
            cw.writerow(self.args.columns.split(','))

            # Write data to the file
            for record in self.records:
                o = record.output(self.args.columns)
                cw.writerow(o)

class Param:
    """Basic Parameter structure"""
    def __init__(self, name, default, description, prefix):
        self.name = name
        self.default = default
        self.description = description
        self.prefix = prefix

    def arg(self):
        """The argument to listen for from console"""
        return self.prefix + self.name

    def text(self):
        """Returns the text to output in the help"""
        return self.description + ' Defaults to: ' + self.default

class Params:
    params = {
        'sort',
        'columns',
        'output',
        'groupBy',
        'priority',
        'path'
    }

    def __init__(self, config, arg_parser):
        for name in self.params:
            # for name, opts in self.params.iteritems():

            param = Param(
                name=name,
                description=config.get(name, 'description'),
                default=config.get(name, 'default'),
                prefix=config.get(name, 'prefix')
            )

            arg_parser.add_argument(
                param.arg(),
                default=param.default,
                help=param.text()
            )

        args = arg_parser.parse_args()
        self.fix_delimiters(args)
        self.args = args

    def fix_delimiters(self, args):
        # Convert "tab" output arg to actual character
        if args.output == 'tab':
            args.output = '\t'

# Read configs
config = ConfigParser.ConfigParser()
config.read("config.cfg")
logging.info("Config read.")

# Parse input
arg_parser = argparse.ArgumentParser(
    description='Multi-CSV manipulation/digestion')

params = Params(config, arg_parser)
logging.info("Args parsed. Starting walk.")

records = Records(config, params.args)
records.get()

# Output results
logging.info("Done processing. Generating result file.")

records.write()